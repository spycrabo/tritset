#pragma once
#include <cstdlib>
#include <iostream>
#include <string>


class tritset {
//��������� ���� � ����!
public:
	class trit {
	private:
		char * value;
		int pos;
	public:
		trit(int a, int p); //a - ��������, p - �������
		trit();
		friend class tritset;
		trit& operator= (int val);
		friend std::ostream& operator<< (std::ostream& os, const trit& p);
		friend std::istream& operator >> (std::istream& is, trit& p);
		int getval();
	};
	tritset();
	tritset(int a);
	tritset(int a, int v);
	int �ardinality(int val);
	void trim(int lastIndex);
	trit operator[] (int n) const;
	tritset& operator= (tritset& p);
	tritset operator&(tritset& p);
	tritset operator|(tritset& p);
	tritset operator~();
	friend std::ostream& operator<<(std::ostream& os, const tritset& p);
	friend std::istream& operator>>(std::istream& is, tritset& p);

private:
	char *data;
	int size; //������ � ���������� ������
};

#include "trit.h"

//����������� ������������� �����
tritset::trit::trit(int a, int p)
{
	int m1 = p * 2;
	value = (char *)calloc(sizeof(char), 1);
	if (a != 0) {
		(*value) |= 1 << m1;
	}
	if (a > 0) {
		m1++;
		(*value) |= 1 << m1;
	}
	pos = p;
}

//����������� ��������������� �����
tritset::trit::trit()
{
	value = NULL;
	pos = 0;
}

//���������� ������������ ��� ������
tritset::trit & tritset::trit::operator=(int val)
{
	char m1 = pos * 2;
	if (val != 0) {
		(*value) |= 1 << m1;
		m1++;
		if (val > 0)
			(*value) |= 1 << m1;
		else
			(*value) &= ~(1 << m1);
	}
	else {
		(*value) &= ~(1 << m1);
		m1++;
		(*value) &= ~(1 << m1);
	}

	return *this;
}

//�������������� �������� ����� � ����� ���
int tritset::trit::getval()
{
	char m1 = pos * 2;
	if ((*value >> m1) & 1) {
		m1++;
		if ((*value >> m1) & 1)
			return 1;
		else
			return -1;
	}
	else
		return 0;
}

tritset::tritset()
{
	data = NULL;
	size = 0;
}

//����������� ��������
tritset::tritset(int a)
{
	int mem = (double)a / 4 + 0.75;
	data = (char *)calloc(mem, sizeof(char));
	size = a;
}

//����������� �������� � �������� ��������� ���� ������
tritset::tritset(int a, int v)
{
	int mem = (double)a / 4 + 0.75;
	data = (char *)calloc(mem, sizeof(char));
	size = a;
	for (int i = 0; i < mem; i++) {
		char * value = &(data[i]);
		if (v > 0)
			*value = 255;
		else if (v < 0)
			*value = 85;
		else
			*value = 0;
	}
	

}

//���������� ������, ������������� � �������� ��������
int tritset::�ardinality(int val)
{
	int counter = 0;
	int i = size - 1;
	if (val == 0)
		while ((i >= 0) && ((*this)[i].getval() != 1) && ((*this)[i].getval() != -1))
			i--;
	if (i == 0) return 0;
	for (int j = 0; j <= i; j++)
		if ((*this)[j].getval() == val) counter++;

	return counter;
}

//�������� ������ ������� � lastIndex
void tritset::trim(int lastIndex)
{
	for (int i = lastIndex; i < size; i++)
		(*this)[i] = 0;
}

//���������� ���������� [] ��� ��������
tritset::trit tritset::operator[] (int n) const
{
	trit k(0, 0);
	if (n >= size) return k;
	k.pos = (n) % 4;
	k.value = &((data)[n / 4]);
	return k;
}

//���������� ������������ ���������
tritset & tritset::operator=(tritset & p)
{
	std::swap(data, p.data);
	std::swap(size, p.size);
	return *this;
}

//���������� �
tritset tritset::operator&(tritset & p)
{
	int nsize = (size > p.size) ? size : p.size;
	int v1, v2;
	tritset k(nsize);
	for (int i = 0; i < nsize; i++) {
		v1 = (*this)[i].getval();
		v2 = p[i].getval();
		if (((v1 < v2) ? v1 : v2) == -1)
			k[i] = -1;
		else
			if (v1 + v2 == 2)
				k[i] = 1;
			else
				k[i] = 0;
	}
	return k;
}

//���������� ���
tritset tritset::operator|(tritset & p)
{
	int nsize = (size > p.size) ? size : p.size;
	int v1, v2;
	tritset k(nsize);
	for (int i = 0; i < nsize; i++) {
		v1 = (*this)[i].getval();
		v2 = p[i].getval();
		if (((v1 > v2) ? v1 : v2) == 1)
			k[i] = 1;
		else
			if (v1 + v2 == -2)
				k[i] = -1;
			else
				k[i] = 0;
	}
	return k;
}

//���������� ��
tritset tritset::operator~()
{
	int v1, v2;
	tritset k(size);
	for (int i = 0; i < size; i++) {
		v1 = (*this)[i].getval();
		if (v1 == -1)
			k[i] = 1;
		else
			if (v1 == 1)
				k[i] = -1;
			else
				k[i] = 0;
	}
	return k;
}

//��������� ����� �����
std::ostream & operator<<(std::ostream & os, const tritset::trit & p)
{
	char m1 = p.pos * 2;
	if (((*p.value) >> m1) & 1) {
		m1++;
		if (((*p.value) >> m1) & 1)
			os << "True";
		else
			os << "False";
	}
	else
		os << "Unknown";

	return os;
}

//��������� ���� �����
std::istream & operator >> (std::istream & is, tritset::trit & p)
{
	char x = is.get();
	if (x == 'f')
		p = -1;
	else if (x == 't')
		p = 1;
	else
		p = 0;
	return is;
}

//��������� ����� ��������
std::ostream & operator<<(std::ostream & os, const tritset & p)
{
	for (int i = 0; i < p.size; i++)
		os << p[i] << " ";
	return os;
}

//��������� ���� ��������
std::istream & operator>> (std::istream & is, tritset & p)
{
	delete p.data;
	std::string str;
	std::getline(is, str);
	p.size = str.length();
	int mem = (double)(p.size) / 4 + 0.75;
	p.data = (char *)malloc(sizeof(char) * mem);
	for (int i = 0; i < p.size; i++) {
		if (str[i] == 'f')
			p[i] = -1;
		else if (str[i] == 't')
			p[i] = 1;
		else
			p[i] = 0;
	}
	return is;
}
